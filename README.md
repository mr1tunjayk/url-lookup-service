# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* We have an HTTP proxy that is scanning traffic looking for malware URLs. Before allowing HTTP connections to be made, this proxy asks a service that maintains several databases of malware URLs if the resource being requested is known to contain malware.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Design Diagram ###
![https://go.gliffy.com/go/publish/12277171](./MalwareLookupServiceDesign.png)

### Who do I talk to? ###

* Repo owner: Mritunjay Kumar