package com.mrit.urllookupservice.api;

import com.mrit.urllookupservice.api.models.MalwareOutput;
import com.mrit.urllookupservice.services.LookupService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.is;

public class UrlLookupControllerTest {
    private MockMvc mockMvc;

    @InjectMocks
    UrlLookupController urlLookupControllerMock;

    @Mock
    private LookupService lookupServiceMock;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(urlLookupControllerMock).build();
    }

    @Test
    public void blank_parameter_url_calls_returns_false() throws Exception {
        String urlToTest = "/urlinfo/1/?url=";

        // Mock the service that is injected to the controller
        when(lookupServiceMock.getMalwareInformation("")).thenReturn(getMockedResponse(false));

        mockMvc.perform(get(urlToTest).accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.isMalware", is(false)));
    }

    @Test
    public void non_malware_url_parameter_returns_false() throws Exception {
        String urlToTest = "/urlinfo/1/?url=non_malware_url";

        // Mock the service that is injected to the controller
        when(lookupServiceMock.getMalwareInformation("non_malware_url")).thenReturn(getMockedResponse(false));

        mockMvc.perform(get(urlToTest).accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.isMalware", is(false)));
    }

    @Test
    public void malware_url_parameter_returns_true() throws Exception {
        String urlToTest = "/urlinfo/1/?url=malware_url";

        // Mock the service that is injected to the controller
        when(lookupServiceMock.getMalwareInformation("malware_url")).thenReturn(getMockedResponse(true));

        mockMvc.perform(get(urlToTest).accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.isMalware", is(true)));
    }

    private MalwareOutput getMockedResponse(boolean isMalware){
        return new MalwareOutput(isMalware);
    }
}
