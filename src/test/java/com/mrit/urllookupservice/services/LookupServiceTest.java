package com.mrit.urllookupservice.services;

import com.mrit.urllookupservice.api.models.Malware;
import com.mrit.urllookupservice.api.models.MalwareOutput;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

public class LookupServiceTest {
    @InjectMocks
    LookupService lookupServiceMock;

    @Mock
    private IMalwareRepository malwareRepositoryMock;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void existing_malware_url_should_return_true_output() throws Exception {
        String existingMalwareUrl = "existing_malware_url";

        when(malwareRepositoryMock.findByUrl(existingMalwareUrl)).thenReturn(new Malware());
        MalwareOutput output = lookupServiceMock.getMalwareInformation(existingMalwareUrl);

        assertThat(output.isMalware()).isEqualTo(true);
    }

    @Test
    public void not_existing_malware_url_should_return_false_output() throws Exception {
        String notExistingMalwareUrl = "not_existing_malware_url";

        when(malwareRepositoryMock.findByUrl(notExistingMalwareUrl)).thenReturn(null);
        MalwareOutput output = lookupServiceMock.getMalwareInformation(notExistingMalwareUrl);

        assertThat(output.isMalware()).isEqualTo(false);
    }

    @Test
    public void null_url_should_return_false_output() throws Exception {
        String nullUrl = null;

        MalwareOutput output = lookupServiceMock.getMalwareInformation(nullUrl);

        assertThat(output.isMalware()).isEqualTo(false);
    }

    @Test
    public void empty_string_url_should_return_false_output() throws Exception {
        String emptyStringUrl = "";

        MalwareOutput output = lookupServiceMock.getMalwareInformation(emptyStringUrl);

        assertThat(output.isMalware()).isEqualTo(false);
    }
}
