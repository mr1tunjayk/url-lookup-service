package com.mrit.urllookupservice.services;

import com.mrit.urllookupservice.api.models.MalwareOutput;
import com.mrit.urllookupservice.api.models.Malware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class LookupService {

    @Autowired
    IMalwareRepository malwareRepository;

    /**
     * This method queries MongoDB and gets the Malware document if present in the database.
     * @param url
     * @return
     */
    public MalwareOutput getMalwareInformation(final String url)
    {
        // If url is null or contains nothing,
        // then return null to represent that this URL is no malware.
        if(url == null || url.isEmpty()) return translateDataToOutputModel(null);

        Malware malware = malwareRepository.findByUrl(url);
        return translateDataToOutputModel(malware);
    }

    /**
     * Wrote this private method to keep domain and output(response) models decoupled.
     * @param dataModel
     * @return
     */
    private MalwareOutput translateDataToOutputModel(final Malware dataModel) {
        // if "dataModel != null" that means URL is found in the Malware documents in mongodb
        return new MalwareOutput(dataModel != null);
    }
}
