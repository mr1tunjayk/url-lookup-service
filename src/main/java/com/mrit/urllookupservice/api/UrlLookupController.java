package com.mrit.urllookupservice.api;

import com.mrit.urllookupservice.api.models.MalwareOutput;
import com.mrit.urllookupservice.services.LookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// Below, "/urlinfo/1" is "base" url for the whole class.
// Here, I am assuming "1" is the API version.
@RequestMapping("/urlinfo/1")
@RestController
public class UrlLookupController {
    @Autowired
    LookupService lookupService;

    // Relative path from the "base" url
    @RequestMapping(value = "",
            method = RequestMethod.GET,
            consumes = MediaType.ALL_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MalwareOutput> getMalwareInformation(@RequestParam(value = "url") final String url) {
        MalwareOutput outputModel = lookupService.getMalwareInformation(url);
        return ResponseEntity.status(HttpStatus.OK).body(outputModel);
    }
}
